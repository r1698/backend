package forms

import "github.com/gofiber/fiber/v2"

type GenerateKey struct {
	ComputerName string `json:"computer_name"`
}

type DecryptKey struct {
	Key          *fiber.FormFile `form:"key"`
	ComputerName string          `form:"computer_name"`
}
