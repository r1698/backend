package main

import (
	"ransom_server/config"
	"ransom_server/routes"
)

func main() {
	config.Init()
	routes.CreateRoutes()
}
