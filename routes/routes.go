package routes

import (
	"ransom_server/controllers"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func CreateRoutes() {
	app := fiber.New()

	app.Use(cors.New())

	keyController := controllers.KeyController{}

	app.Post("/generate-key", keyController.GenerateKey)
	app.Post("/decrypt-key", keyController.DecryptKey)

	app.Listen(":3000")
}
